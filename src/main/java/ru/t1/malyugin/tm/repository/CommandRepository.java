package ru.t1.malyugin.tm.repository;

import ru.t1.malyugin.tm.api.ICommandRepository;
import ru.t1.malyugin.tm.constant.ArgumentConst;
import ru.t1.malyugin.tm.constant.CommandConst;
import ru.t1.malyugin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, "Show command list.", ArgumentConst.HELP);

    private static final Command VERSION = new Command(CommandConst.VERSION, "Show version info.", ArgumentConst.VERSION);

    private static final Command ABOUT = new Command(CommandConst.ABOUT, "Show Author info.", ArgumentConst.ABOUT);

    private static final Command INFO = new Command(CommandConst.INFO, "Show system info.", ArgumentConst.INFO);

    private static final Command EXIT = new Command(CommandConst.EXIT, "Close Application.");

    private static final Command[] COMMANDS = new Command[] {
            HELP, VERSION, ABOUT, INFO, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}